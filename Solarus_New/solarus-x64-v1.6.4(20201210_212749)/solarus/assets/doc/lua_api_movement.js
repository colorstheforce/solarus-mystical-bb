var lua_api_movement =
[
    [ "Functions of sol.movement", "lua_api_movement.html#lua_api_movement_functions", [
      [ "sol.movement.create(movement_type)", "lua_api_movement.html#lua_api_movement_create", null ]
    ] ],
    [ "Methods of all movement types", "lua_api_movement.html#lua_api_movement_methods", [
      [ "movement:start(object_to_move, [callback])", "lua_api_movement.html#lua_api_movement_start", null ],
      [ "movement:stop()", "lua_api_movement.html#lua_api_movement_stop", null ],
      [ "movement:get_xy()", "lua_api_movement.html#lua_api_movement_get_xy", null ],
      [ "movement:set_xy(x, y)", "lua_api_movement.html#lua_api_movement_set_xy", null ],
      [ "movement:is_suspended()", "lua_api_movement.html#lua_api_movement_is_suspended", null ],
      [ "movement:get_ignore_suspend()", "lua_api_movement.html#lua_api_movement_get_ignore_suspend", null ],
      [ "movement:set_ignore_suspend([ignore])", "lua_api_movement.html#lua_api_movement_set_ignore_suspend", null ],
      [ "movement:get_ignore_obstacles()", "lua_api_movement.html#lua_api_movement_get_ignore_obstacles", null ],
      [ "movement:set_ignore_obstacles([ignore_obstacles])", "lua_api_movement.html#lua_api_movement_set_ignore_obstacles", null ],
      [ "movement:get_direction4()", "lua_api_movement.html#lua_api_movement_get_direction4", null ]
    ] ],
    [ "Events of all movement types", "lua_api_movement.html#lua_api_movement_events", [
      [ "movement:on_position_changed()", "lua_api_movement.html#lua_api_movement_on_position_changed", null ],
      [ "movement:on_obstacle_reached()", "lua_api_movement.html#lua_api_movement_on_obstacle_reached", null ],
      [ "movement:on_changed()", "lua_api_movement.html#lua_api_movement_on_changed", null ],
      [ "movement:on_finished()", "lua_api_movement.html#lua_api_movement_on_finished", null ]
    ] ],
    [ "Straight movement", "lua_api_straight_movement.html", [
      [ "Methods inherited from movement", "lua_api_straight_movement.html#lua_api_straight_movement_inherited_methods", null ],
      [ "Methods of the type straight movement", "lua_api_straight_movement.html#lua_api_straight_movement_methods", [
        [ "straight_movement:get_speed()", "lua_api_straight_movement.html#lua_api_straight_movement_get_speed", null ],
        [ "straight_movement:set_speed(speed)", "lua_api_straight_movement.html#lua_api_straight_movement_set_speed", null ],
        [ "straight_movement:get_angle()", "lua_api_straight_movement.html#lua_api_straight_movement_get_angle", null ],
        [ "straight_movement:set_angle(angle)", "lua_api_straight_movement.html#lua_api_straight_movement_set_angle", null ],
        [ "straight_movement:get_max_distance()", "lua_api_straight_movement.html#lua_api_straight_movement_get_max_distance", null ],
        [ "straight_movement:set_max_distance(max_distance)", "lua_api_straight_movement.html#lua_api_straight_movement_set_max_distance", null ],
        [ "straight_movement:is_smooth()", "lua_api_straight_movement.html#lua_api_straight_movement_is_smooth", null ],
        [ "straight_movement:set_smooth([smooth])", "lua_api_straight_movement.html#lua_api_straight_movement_set_smooth", null ]
      ] ],
      [ "Events inherited from movement", "lua_api_straight_movement.html#lua_api_straight_movement_inherited_events", null ]
    ] ],
    [ "Random movement", "lua_api_random_movement.html", [
      [ "Methods inherited from movement", "lua_api_random_movement.html#lua_api_random_movement_inherited_methods", null ],
      [ "Methods of the type random movement", "lua_api_random_movement.html#lua_api_random_movement_methods", [
        [ "random_movement:get_speed()", "lua_api_random_movement.html#lua_api_random_movement_get_speed", null ],
        [ "random_movement:set_speed(speed)", "lua_api_random_movement.html#lua_api_random_movement_set_speed", null ],
        [ "random_movement:get_angle()", "lua_api_random_movement.html#lua_api_random_movement_get_angle", null ],
        [ "random_movement:get_max_distance()", "lua_api_random_movement.html#lua_api_random_movement_get_max_distance", null ],
        [ "random_movement:set_max_distance(max_distance)", "lua_api_random_movement.html#lua_api_random_movement_set_max_distance", null ],
        [ "random_movement:is_smooth()", "lua_api_random_movement.html#lua_api_random_movement_is_smooth", null ],
        [ "random_movement:set_smooth([smooth])", "lua_api_random_movement.html#lua_api_random_movement_set_smooth", null ]
      ] ],
      [ "Events inherited from movement", "lua_api_random_movement.html#lua_api_random_movement_inherited_events", null ]
    ] ],
    [ "Target movement", "lua_api_target_movement.html", [
      [ "Methods inherited from movement", "lua_api_target_movement.html#lua_api_target_movement_inherited_methods", null ],
      [ "Methods of the type target movement", "lua_api_target_movement.html#lua_api_target_movement_methods", [
        [ "target_movement:set_target(x, y), target_movement:set_target(entity, [x, y])", "lua_api_target_movement.html#lua_api_target_movement_set_target", null ],
        [ "target_movement:get_speed()", "lua_api_target_movement.html#lua_api_target_movement_get_speed", null ],
        [ "target_movement:set_speed(speed)", "lua_api_target_movement.html#lua_api_target_movement_set_speed", null ],
        [ "target_movement:get_angle()", "lua_api_target_movement.html#lua_api_target_movement_get_angle", null ],
        [ "target_movement:is_smooth()", "lua_api_target_movement.html#lua_api_target_movement_is_smooth", null ],
        [ "target_movement:set_smooth([smooth])", "lua_api_target_movement.html#lua_api_target_movement_set_smooth", null ]
      ] ],
      [ "Events inherited from movement", "lua_api_target_movement.html#lua_api_target_movement_inherited_events", null ]
    ] ],
    [ "Path movement", "lua_api_path_movement.html", [
      [ "Methods inherited from movement", "lua_api_path_movement.html#lua_api_path_movement_inherited_methods", null ],
      [ "Methods of the type path movement", "lua_api_path_movement.html#lua_api_path_movement_methods", [
        [ "path_movement:get_path()", "lua_api_path_movement.html#lua_api_path_movement_get_path", null ],
        [ "path_movement:set_path(path)", "lua_api_path_movement.html#lua_api_path_movement_set_path", null ],
        [ "path_movement:get_speed()", "lua_api_path_movement.html#lua_api_path_movement_get_speed", null ],
        [ "path_movement:set_speed(speed)", "lua_api_path_movement.html#lua_api_path_movement_set_speed", null ],
        [ "path_movement:get_loop()", "lua_api_path_movement.html#lua_api_path_movement_get_loop", null ],
        [ "path_movement:set_loop([loop])", "lua_api_path_movement.html#lua_api_path_movement_set_loop", null ],
        [ "path_movement:get_snap_to_grid()", "lua_api_path_movement.html#lua_api_path_movement_get_snap_to_grid", null ],
        [ "path_movement:set_snap_to_grid([snap])", "lua_api_path_movement.html#lua_api_path_movement_set_snap_to_grid", null ],
        [ "path_movement:get_angle()", "lua_api_path_movement.html#lua_api_path_movement_get_angle", null ]
      ] ],
      [ "Events inherited from movement", "lua_api_path_movement.html#lua_api_path_movement_inherited_events", null ]
    ] ],
    [ "Random path movement", "lua_api_random_path_movement.html", [
      [ "Methods inherited from movement", "lua_api_random_path_movement.html#lua_api_random_path_movement_inherited_methods", null ],
      [ "Methods of the type random path movement", "lua_api_random_path_movement.html#lua_api_random_path_movement_methods", [
        [ "random_path_movement:get_speed()", "lua_api_random_path_movement.html#lua_api_random_path_movement_get_speed", null ],
        [ "random_path_movement:set_speed(speed)", "lua_api_random_path_movement.html#lua_api_random_path_movement_set_speed", null ],
        [ "random_path_movement:get_angle()", "lua_api_random_path_movement.html#lua_api_random_path_movement_get_angle", null ]
      ] ],
      [ "Events inherited from movement", "lua_api_random_path_movement.html#lua_api_random_path_movement_inherited_events", null ]
    ] ],
    [ "Path finding movement", "lua_api_path_finding_movement.html", [
      [ "Methods inherited from movement", "lua_api_path_finding_movement.html#lua_api_path_finding_movement_inherited_methods", null ],
      [ "Methods of the type path finding movement", "lua_api_path_finding_movement.html#lua_api_path_finding_movement_methods", [
        [ "path_finding_movement:set_target(entity)", "lua_api_path_finding_movement.html#lua_api_path_finding_movement_set_target", null ],
        [ "path_finding_movement:get_speed()", "lua_api_path_finding_movement.html#lua_api_path_finding_movement_get_speed", null ],
        [ "path_finding_movement:set_speed(speed)", "lua_api_path_finding_movement.html#lua_api_path_finding_movement_set_speed", null ],
        [ "path_finding_movement:get_angle()", "lua_api_path_finding_movement.html#lua_api_path_finding_movement_get_angle", null ]
      ] ],
      [ "Events inherited from movement", "lua_api_path_finding_movement.html#lua_api_path_finding_movement_inherited_events", null ]
    ] ],
    [ "Circle movement", "lua_api_circle_movement.html", [
      [ "Methods inherited from movement", "lua_api_circle_movement.html#lua_api_circle_movement_inherited_methods", null ],
      [ "Methods of the type circle movement", "lua_api_circle_movement.html#lua_api_circle_movement_methods", [
        [ "circle_movement:get_center()", "lua_api_circle_movement.html#lua_api_movement_get_center", null ],
        [ "circle_movement:set_center(x, y), circle_movement:set_center(entity, [dx, dy])", "lua_api_circle_movement.html#lua_api_circle_movement_set_center", null ],
        [ "circle_movement:get_radius()", "lua_api_circle_movement.html#lua_api_circle_movement_get_radius", null ],
        [ "circle_movement:set_radius(radius)", "lua_api_circle_movement.html#lua_api_circle_movement_set_radius", null ],
        [ "circle_movement:get_radius_speed()", "lua_api_circle_movement.html#lua_api_circle_movement_get_radius_speed", null ],
        [ "circle_movement:set_radius_speed(radius_speed)", "lua_api_circle_movement.html#lua_api_circle_movement_set_radius_speed", null ],
        [ "circle_movement:is_clockwise()", "lua_api_circle_movement.html#lua_api_circle_movement_is_clockwise", null ],
        [ "circle_movement:set_clockwise([clockwise])", "lua_api_circle_movement.html#lua_api_circle_movement_set_clockwise", null ],
        [ "circle_movement:get_angle_from_center()", "lua_api_circle_movement.html#lua_api_circle_movement_get_angle_from_center", null ],
        [ "circle_movement:set_angle_from_center(angle_from_center)", "lua_api_circle_movement.html#lua_api_circle_movement_set_angle_from_center", null ],
        [ "circle_movement:get_angular_speed()", "lua_api_circle_movement.html#lua_api_circle_movement_get_angular_speed", null ],
        [ "circle_movement:set_angular_speed(angular_speed)", "lua_api_circle_movement.html#lua_api_circle_movement_set_angular_speed", null ],
        [ "circle_movement:get_max_rotations()", "lua_api_circle_movement.html#lua_api_circle_movement_get_max_rotations", null ],
        [ "circle_movement:set_max_rotations(max_rotations)", "lua_api_circle_movement.html#lua_api_circle_movement_set_max_rotations", null ],
        [ "circle_movement:get_duration()", "lua_api_circle_movement.html#lua_api_circle_movement_get_duration", null ],
        [ "circle_movement:set_duration(duration)", "lua_api_circle_movement.html#lua_api_circle_movement_set_duration", null ],
        [ "circle_movement:get_loop_delay()", "lua_api_circle_movement.html#lua_api_circle_movement_get_loop_delay", null ],
        [ "circle_movement:set_loop_delay(loop_delay)", "lua_api_circle_movement.html#lua_api_circle_movement_set_loop_delay", null ]
      ] ],
      [ "Events inherited from movement", "lua_api_circle_movement.html#lua_api_circle_movement_inherited_events", null ],
      [ "Deprecated methods of the type circle movement", "lua_api_circle_movement.html#lua_api_circle_movement_methods_deprecated", [
        [ "circle_movement:get_initial_angle()", "lua_api_circle_movement.html#lua_api_circle_movement_get_initial_angle", null ],
        [ "circle_movement:set_initial_angle(initial_angle)", "lua_api_circle_movement.html#lua_api_circle_movement_set_initial_angle", null ],
        [ "circle_movement:get_angle_speed()", "lua_api_circle_movement.html#lua_api_circle_movement_get_angle_speed", null ],
        [ "circle_movement:set_angle_speed(angle_speed)", "lua_api_circle_movement.html#lua_api_circle_movement_set_angle_speed", null ]
      ] ]
    ] ],
    [ "Jump movement", "lua_api_jump_movement.html", [
      [ "Methods inherited from movement", "lua_api_jump_movement.html#lua_api_jump_movement_inherited_methods", null ],
      [ "Methods of the type jump movement", "lua_api_jump_movement.html#lua_api_jump_movement_methods", [
        [ "jump_movement:get_direction8()", "lua_api_jump_movement.html#lua_api_jump_movement_get_direction8", null ],
        [ "jump_movement:set_direction8(direction8)", "lua_api_jump_movement.html#lua_api_jump_movement_set_direction8", null ],
        [ "jump_movement:get_distance()", "lua_api_jump_movement.html#lua_api_jump_movement_get_distance", null ],
        [ "jump_movement:set_distance(distance)", "lua_api_jump_movement.html#lua_api_jump_movement_set_distance", null ],
        [ "jump_movement:get_speed()", "lua_api_jump_movement.html#lua_api_jump_movement_get_speed", null ],
        [ "jump_movement:set_speed(speed)", "lua_api_jump_movement.html#lua_api_jump_movement_set_speed", null ]
      ] ],
      [ "Events inherited from movement", "lua_api_jump_movement.html#lua_api_jump_movement_inherited_events", null ]
    ] ],
    [ "Pixel movement", "lua_api_pixel_movement.html", [
      [ "Methods inherited from movement", "lua_api_pixel_movement.html#lua_api_pixel_movement_inherited_methods", null ],
      [ "Methods of the type pixel movement", "lua_api_pixel_movement.html#lua_api_pixel_movement_methods", [
        [ "pixel_movement:get_trajectory()", "lua_api_pixel_movement.html#lua_api_pixel_movement_get_trajectory", null ],
        [ "pixel_movement:set_trajectory(trajectory)", "lua_api_pixel_movement.html#lua_api_pixel_movement_set_trajectory", null ],
        [ "pixel_movement:get_loop()", "lua_api_pixel_movement.html#lua_api_pixel_movement_get_loop", null ],
        [ "pixel_movement:set_loop([loop])", "lua_api_pixel_movement.html#lua_api_pixel_movement_set_loop", null ],
        [ "pixel_movement:get_delay()", "lua_api_pixel_movement.html#lua_api_pixel_movement_get_delay", null ],
        [ "pixel_movement:set_delay(delay)", "lua_api_pixel_movement.html#lua_api_pixel_movement_set_delay", null ]
      ] ],
      [ "Events inherited from movement", "lua_api_pixel_movement.html#lua_api_pixel_movement_inherited_events", null ]
    ] ]
];