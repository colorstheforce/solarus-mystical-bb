/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Solarus Quests", "index.html", [
    [ "Solarus 1.6 - Lua API reference", "lua_api.html", "lua_api" ],
    [ "Solarus 1.6 - Quest data files specification", "quest.html", "quest" ],
    [ "How to translate a quest", "translation.html", [
      [ "Images", "translation.html#images", null ],
      [ "Strings", "translation.html#strings", null ],
      [ "Dialogs", "translation.html#dialogs", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"index.html",
"lua_api_enemy.html#lua_api_enemy_create_enemy",
"lua_api_hero.html#lua_api_hero_get_sword_sprite_id",
"lua_api_map.html#lua_api_map_on_suspended",
"lua_api_state.html#lua_api_state_on_attacked_enemy",
"quest_tileset_data_file.html#quest_tileset_data_file_syntax"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';