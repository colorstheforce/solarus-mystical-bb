var lua_api_drawable =
[
    [ "Methods of all drawable types", "lua_api_drawable.html#lua_api_drawable_methods", [
      [ "drawable:draw(dst_surface, [x, y])", "lua_api_drawable.html#lua_api_drawable_draw", null ],
      [ "drawable:draw_region(region_x, region_y, region_width, region_height, dst_surface, [x, y])", "lua_api_drawable.html#lua_api_drawable_draw_region", null ],
      [ "drawable:get_rotation()", "lua_api_drawable.html#lua_api_drawable_get_rotation", null ],
      [ "drawable:set_rotation(rotation)", "lua_api_drawable.html#lua_api_drawable_set_rotation", null ],
      [ "drawable:get_scale()", "lua_api_drawable.html#lua_api_drawable_get_scale", null ],
      [ "drawable:set_scale(x, y)", "lua_api_drawable.html#lua_api_drawable_set_scale", null ],
      [ "drawable:get_transformation_origin()", "lua_api_drawable.html#lua_api_drawable_get_transformation_origin", null ],
      [ "drawable:set_transformation_origin(x, y)", "lua_api_drawable.html#lua_api_drawable_set_transformation_origin", null ],
      [ "drawable:get_blend_mode()", "lua_api_drawable.html#lua_api_drawable_get_blend_mode", null ],
      [ "drawable:set_blend_mode(blend_mode)", "lua_api_drawable.html#lua_api_drawable_set_blend_mode", null ],
      [ "drawable:get_shader()", "lua_api_drawable.html#lua_api_drawable_get_shader", null ],
      [ "drawable:set_shader(shader)", "lua_api_drawable.html#lua_api_drawable_set_shader", null ],
      [ "drawable:get_opacity()", "lua_api_drawable.html#lua_api_drawable_get_opacity", null ],
      [ "drawable:set_opacity(opacity)", "lua_api_drawable.html#lua_api_drawable_set_opacity", null ],
      [ "drawable:get_color_modulation()", "lua_api_drawable.html#lua_api_drawable_get_color_modulation", null ],
      [ "drawable:set_color_modulation(color)", "lua_api_drawable.html#lua_api_drawable_set_color_modulation", null ],
      [ "drawable:fade_in([delay], [callback])", "lua_api_drawable.html#lua_api_drawable_fade_in", null ],
      [ "drawable:fade_out([delay], [callback])", "lua_api_drawable.html#lua_api_drawable_fade_out", null ],
      [ "drawable:get_xy()", "lua_api_drawable.html#lua_api_drawable_get_xy", null ],
      [ "drawable:set_xy(x, y)", "lua_api_drawable.html#lua_api_drawable_set_xy", null ],
      [ "drawable:get_movement()", "lua_api_drawable.html#lua_api_drawable_get_movement", null ],
      [ "drawable:stop_movement()", "lua_api_drawable.html#lua_api_drawable_stop_movement", null ]
    ] ],
    [ "Surfaces", "lua_api_surface.html", [
      [ "Functions of sol.surface", "lua_api_surface.html#lua_api_surface_functions", [
        [ "sol.surface.create([width, height])", "lua_api_surface.html#lua_api_surface_create_empty", null ],
        [ "sol.surface.create(file_name, [language_specific])", "lua_api_surface.html#lua_api_surface_create_from_file", null ]
      ] ],
      [ "Methods inherited from drawable", "lua_api_surface.html#lua_api_surface_inherited_methods", null ],
      [ "Methods of the type surface", "lua_api_surface.html#lua_api_surface_methods", [
        [ "surface:get_size()", "lua_api_surface.html#lua_api_surface_get_size", null ],
        [ "surface:clear()", "lua_api_surface.html#lua_api_surface_clear", null ],
        [ "surface:fill_color(color, [x, y, width, height])", "lua_api_surface.html#lua_api_surface_fill_color", null ],
        [ "surface:get_pixels()", "lua_api_surface.html#lua_api_surface_get_pixels", null ],
        [ "surface:set_pixels(pixels)", "lua_api_surface.html#lua_api_surface_set_pixels", null ],
        [ "surface:gl_bind_as_target()", "lua_api_surface.html#lua_api_surface_gl_bind_as_target", null ],
        [ "surface:gl_bind_as_texture()", "lua_api_surface.html#lua_api_surface_gl_bind_as_texture", null ]
      ] ]
    ] ],
    [ "Text surfaces", "lua_api_text_surface.html", [
      [ "Functions of sol.text_surface", "lua_api_text_surface.html#lua_api_text_surface_functions", [
        [ "sol.text_surface.create([properties])", "lua_api_text_surface.html#lua_api_text_surface_create", null ],
        [ "sol.text_surface.get_predicted_size(font_id, font_size, text)", "lua_api_text_surface.html#lua_api_text_surface_get_predicted_size", null ]
      ] ],
      [ "Methods inherited from drawable", "lua_api_text_surface.html#lua_api_text_surface_inherited_methods", null ],
      [ "Methods of the type text surface", "lua_api_text_surface.html#lua_api_text_surface_methods", [
        [ "text_surface:get_horizontal_alignment()", "lua_api_text_surface.html#lua_api_text_surface_get_horizontal_alignment", null ],
        [ "text_surface:set_horizontal_alignment(horizontal_alignment)", "lua_api_text_surface.html#lua_api_text_surface_set_horizontal_alignment", null ],
        [ "text_surface:get_vertical_alignment()", "lua_api_text_surface.html#lua_api_text_surface_get_vertical_alignment", null ],
        [ "text_surface:set_vertical_alignment(vertical_alignment)", "lua_api_text_surface.html#lua_api_text_surface_set_vertical_alignment", null ],
        [ "text_surface:get_font()", "lua_api_text_surface.html#lua_api_text_surface_get_font", null ],
        [ "text_surface:set_font(font_id)", "lua_api_text_surface.html#lua_api_text_surface_set_font", null ],
        [ "text_surface:get_rendering_mode()", "lua_api_text_surface.html#lua_api_text_surface_get_rendering_mode", null ],
        [ "text_surface:set_rendering_mode(rendering_mode)", "lua_api_text_surface.html#lua_api_text_surface_set_rendering_mode", null ],
        [ "text_surface:get_color()", "lua_api_text_surface.html#lua_api_text_surface_get_color", null ],
        [ "text_surface:set_color(color)", "lua_api_text_surface.html#lua_api_text_surface_set_color", null ],
        [ "text_surface:get_font_size()", "lua_api_text_surface.html#lua_api_text_surface_get_font_size", null ],
        [ "text_surface:set_font_size(font_size)", "lua_api_text_surface.html#lua_api_text_surface_set_font_size", null ],
        [ "text_surface:get_text()", "lua_api_text_surface.html#lua_api_text_surface_get_text", null ],
        [ "text_surface:set_text([text])", "lua_api_text_surface.html#lua_api_text_surface_set_text", null ],
        [ "text_surface:set_text_key(key)", "lua_api_text_surface.html#lua_api_text_surface_set_text_key", null ],
        [ "text_surface:get_size()", "lua_api_text_surface.html#lua_api_text_surface_get_size", null ]
      ] ]
    ] ],
    [ "Sprites", "lua_api_sprite.html", [
      [ "Functions of sol.sprite", "lua_api_sprite.html#lua_api_sprite_functions", [
        [ "sol.sprite.create(animation_set_id)", "lua_api_sprite.html#lua_api_sprite_create", null ]
      ] ],
      [ "Methods inherited from drawable", "lua_api_sprite.html#lua_api_sprite_inherited_methods", null ],
      [ "Methods of the type sprite", "lua_api_sprite.html#lua_api_sprite_methods", [
        [ "sprite:get_animation_set()", "lua_api_sprite.html#lua_api_sprite_get_animation_set", null ],
        [ "sprite:has_animation(animation_name)", "lua_api_sprite.html#lua_api_sprite_has_animation", null ],
        [ "sprite:get_animation()", "lua_api_sprite.html#lua_api_sprite_get_animation", null ],
        [ "sprite:set_animation(animation_name, [next_action])", "lua_api_sprite.html#lua_api_sprite_set_animation", null ],
        [ "sprite:stop_animation()", "lua_api_sprite.html#lua_api_sprite_stop_animation", null ],
        [ "sprite:is_animation_started()", "lua_api_sprite.html#lua_api_sprite_is_animation_started", null ],
        [ "sprite:get_direction()", "lua_api_sprite.html#lua_api_sprite_get_direction", null ],
        [ "sprite:set_direction(direction)", "lua_api_sprite.html#lua_api_sprite_set_direction", null ],
        [ "sprite:get_num_directions([animation_name])", "lua_api_sprite.html#lua_api_sprite_get_num_directions", null ],
        [ "sprite:get_frame()", "lua_api_sprite.html#lua_api_sprite_get_frame", null ],
        [ "sprite:set_frame(frame)", "lua_api_sprite.html#lua_api_sprite_set_frame", null ],
        [ "sprite:get_num_frames([animation_name, direction])", "lua_api_sprite.html#lua_api_sprite_get_num_frames", null ],
        [ "sprite:get_frame_delay([animation_name])", "lua_api_sprite.html#lua_api_sprite_get_frame_delay", null ],
        [ "sprite:set_frame_delay(delay)", "lua_api_sprite.html#lua_api_sprite_set_frame_delay", null ],
        [ "sprite:get_size([animation_name, direction])", "lua_api_sprite.html#lua_api_sprite_get_size", null ],
        [ "sprite:get_origin([animation_name, direction])", "lua_api_sprite.html#lua_api_sprite_get_origin", null ],
        [ "sprite:get_frame_src_xy([animation_name, direction, frame])", "lua_api_sprite.html#lua_api_sprite_get_frame_src_xy", null ],
        [ "sprite:is_paused()", "lua_api_sprite.html#lua_api_sprite_is_paused", null ],
        [ "sprite:set_paused([paused])", "lua_api_sprite.html#lua_api_sprite_set_paused", null ],
        [ "sprite:get_ignore_suspend()", "lua_api_sprite.html#lua_api_sprite_get_ignore_suspend", null ],
        [ "sprite:set_ignore_suspend([ignore])", "lua_api_sprite.html#lua_api_sprite_set_ignore_suspend", null ],
        [ "sprite:synchronize([reference_sprite])", "lua_api_sprite.html#lua_api_sprite_synchronize", null ]
      ] ],
      [ "Events of the type sprite", "lua_api_sprite.html#lua_api_sprite_events", [
        [ "sprite:on_animation_finished(animation)", "lua_api_sprite.html#lua_api_sprite_on_animation_finished", null ],
        [ "sprite:on_animation_changed(animation)", "lua_api_sprite.html#lua_api_sprite_on_animation_changed", null ],
        [ "sprite:on_direction_changed(animation, direction)", "lua_api_sprite.html#lua_api_sprite_on_direction_changed", null ],
        [ "sprite:on_frame_changed(animation, frame)", "lua_api_sprite.html#lua_api_sprite_on_frame_changed", null ]
      ] ]
    ] ]
];