var lua_api_entity =
[
    [ "Overview", "lua_api_entity.html#lua_api_entity_overview", null ],
    [ "Methods of all entity types", "lua_api_entity.html#lua_api_entity_methods", [
      [ "entity:get_type()", "lua_api_entity.html#lua_api_entity_get_type", null ],
      [ "entity:get_map()", "lua_api_entity.html#lua_api_entity_get_map", null ],
      [ "entity:get_game()", "lua_api_entity.html#lua_api_entity_get_game", null ],
      [ "entity:get_name()", "lua_api_entity.html#lua_api_entity_get_name", null ],
      [ "entity:exists()", "lua_api_entity.html#lua_api_entity_exists", null ],
      [ "entity:remove()", "lua_api_entity.html#lua_api_entity_remove", null ],
      [ "entity:is_enabled()", "lua_api_entity.html#lua_api_entity_is_enabled", null ],
      [ "entity:set_enabled([enabled])", "lua_api_entity.html#lua_api_entity_set_enabled", null ],
      [ "entity:get_size()", "lua_api_entity.html#lua_api_entity_get_size", null ],
      [ "entity:set_size(width, height)", "lua_api_entity.html#lua_api_entity_set_size", null ],
      [ "entity:get_origin()", "lua_api_entity.html#lua_api_entity_get_origin", null ],
      [ "entity:set_origin(origin_x, origin_y)", "lua_api_entity.html#lua_api_entity_set_origin", null ],
      [ "entity:get_position()", "lua_api_entity.html#lua_api_entity_get_position", null ],
      [ "entity:set_position(x, y, [layer])", "lua_api_entity.html#lua_api_entity_set_position", null ],
      [ "entity:get_center_position()", "lua_api_entity.html#lua_api_entity_get_center_position", null ],
      [ "entity:get_facing_position()", "lua_api_entity.html#lua_api_entity_get_facing_position", null ],
      [ "entity:get_facing_entity()", "lua_api_entity.html#lua_api_entity_get_facing_entity", null ],
      [ "entity:get_ground_position()", "lua_api_entity.html#lua_api_entity_get_ground_position", null ],
      [ "entity:get_ground_below()", "lua_api_entity.html#lua_api_entity_get_ground_below", null ],
      [ "entity:get_bounding_box()", "lua_api_entity.html#lua_api_entity_get_bounding_box", null ],
      [ "entity:get_max_bounding_box()", "lua_api_entity.html#lua_api_entity_get_max_bounding_box", null ],
      [ "entity:get_layer()", "lua_api_entity.html#lua_api_entity_get_layer", null ],
      [ "entity:set_layer(layer)", "lua_api_entity.html#lua_api_entity_set_layer", null ],
      [ "entity:overlaps(x, y, [width, height])", "lua_api_entity.html#lua_api_entity_overlaps_rectangle", null ],
      [ "entity:overlaps(other_entity, [collision_mode, [entity_sprite, [other_entity_sprite]]])", "lua_api_entity.html#lua_api_entity_overlaps_entity", null ],
      [ "entity:get_distance(x, y), entity:get_distance(other_entity)", "lua_api_entity.html#lua_api_entity_get_distance", null ],
      [ "entity:get_angle(x, y), entity:get_angle(other_entity)", "lua_api_entity.html#lua_api_entity_get_angle", null ],
      [ "entity:get_direction4_to(x, y), entity:get_direction4_to(other_entity)", "lua_api_entity.html#lua_api_entity_get_direction4_to", null ],
      [ "entity:get_direction8_to(x, y), entity:get_direction8_to(other_entity)", "lua_api_entity.html#lua_api_entity_get_direction8_to", null ],
      [ "entity:snap_to_grid()", "lua_api_entity.html#lua_api_entity_snap_to_grid", null ],
      [ "entity:bring_to_front()", "lua_api_entity.html#lua_api_entity_bring_to_front", null ],
      [ "entity:bring_to_back()", "lua_api_entity.html#lua_api_entity_bring_to_back", null ],
      [ "entity:is_drawn_in_y_order()", "lua_api_entity.html#lua_api_entity_is_drawn_in_y_order", null ],
      [ "entity:set_drawn_in_y_order([y_order])", "lua_api_entity.html#lua_api_entity_set_drawn_in_y_order", null ],
      [ "entity:get_optimization_distance()", "lua_api_entity.html#lua_api_entity_get_optimization_distance", null ],
      [ "entity:set_optimization_distance(optimization_distance)", "lua_api_entity.html#lua_api_entity_set_optimization_distance", null ],
      [ "entity:is_in_same_region(other_entity)", "lua_api_entity.html#lua_api_entity_is_in_same_region", null ],
      [ "entity:test_obstacles([dx, dy, [layer]])", "lua_api_entity.html#lua_api_entity_test_obstacles", null ],
      [ "entity:get_sprite([name])", "lua_api_entity.html#lua_api_entity_get_sprite", null ],
      [ "entity:get_sprites()", "lua_api_entity.html#lua_api_entity_get_sprites", null ],
      [ "entity:create_sprite(animation_set_id, [sprite_name])", "lua_api_entity.html#lua_api_entity_create_sprite", null ],
      [ "entity:remove_sprite([sprite])", "lua_api_entity.html#lua_api_entity_remove_sprite", null ],
      [ "entity:bring_sprite_to_front(sprite)", "lua_api_entity.html#lua_api_entity_bring_sprite_to_front", null ],
      [ "entity:bring_sprite_to_back(sprite)", "lua_api_entity.html#lua_api_entity_bring_sprite_to_back", null ],
      [ "entity:is_visible()", "lua_api_entity.html#lua_api_entity_is_visible", null ],
      [ "entity:set_visible([visible])", "lua_api_entity.html#lua_api_entity_set_visible", null ],
      [ "entity:get_draw_override()", "lua_api_entity.html#lua_api_entity_get_draw_override", null ],
      [ "entity:set_draw_override(draw_override)", "lua_api_entity.html#lua_api_entity_set_draw_override", null ],
      [ "entity:get_weight()", "lua_api_entity.html#lua_api_entity_get_weight", null ],
      [ "entity:set_weight(weight)", "lua_api_entity.html#lua_api_entity_set_weight", null ],
      [ "entity:get_controlling_stream()", "lua_api_entity.html#lua_api_entity_get_controlling_stream", null ],
      [ "entity:get_movement()", "lua_api_entity.html#lua_api_entity_get_movement", null ],
      [ "entity:stop_movement()", "lua_api_entity.html#lua_api_entity_stop_movement", null ],
      [ "entity:get_property(key)", "lua_api_entity.html#lua_api_entity_get_property", null ],
      [ "entity:set_property(key, value)", "lua_api_entity.html#lua_api_entity_set_property", null ],
      [ "entity:get_properties()", "lua_api_entity.html#lua_api_entity_get_properties", null ],
      [ "entity:set_properties(properties)", "lua_api_entity.html#lua_api_entity_set_properties", null ]
    ] ],
    [ "Events of all entity types", "lua_api_entity.html#lua_api_entity_events", [
      [ "entity:on_created()", "lua_api_entity.html#lua_api_entity_on_created", null ],
      [ "entity:on_removed()", "lua_api_entity.html#lua_api_entity_on_removed", null ],
      [ "entity:on_enabled()", "lua_api_entity.html#lua_api_entity_on_enabled", null ],
      [ "entity:on_disabled()", "lua_api_entity.html#lua_api_entity_on_disabled", null ],
      [ "entity:on_suspended(suspended)", "lua_api_entity.html#lua_api_entity_on_suspended", null ],
      [ "entity:on_position_changed(x, y, layer)", "lua_api_entity.html#lua_api_entity_on_position_changed", null ],
      [ "entity:on_obstacle_reached(movement)", "lua_api_entity.html#lua_api_entity_on_obstacle_reached", null ],
      [ "entity:on_movement_started(movement)", "lua_api_entity.html#lua_api_entity_on_movement_started", null ],
      [ "entity:on_movement_changed(movement)", "lua_api_entity.html#lua_api_entity_on_movement_changed", null ],
      [ "entity:on_movement_finished()", "lua_api_entity.html#lua_api_entity_on_movement_finished", null ],
      [ "entity:on_lifting(carrier, carried_object)", "lua_api_entity.html#lua_api_entity_on_lifting", null ],
      [ "entity:on_pre_draw(camera)", "lua_api_entity.html#lua_api_entity_on_pre_draw", null ],
      [ "entity:on_post_draw(camera)", "lua_api_entity.html#lua_api_entity_on_post_draw", null ]
    ] ],
    [ "Hero", "lua_api_hero.html", [
      [ "Overview", "lua_api_hero.html#lua_api_hero_overview", [
        [ "Hero sprites", "lua_api_hero.html#lua_api_hero_overview_sprites", null ]
      ] ],
      [ "Methods inherited from map entity", "lua_api_hero.html#lua_api_hero_inherited_methods", null ],
      [ "Methods of the type hero", "lua_api_hero.html#lua_api_hero_methods", [
        [ "hero:teleport(map_id, [destination_name, [transition_style]])", "lua_api_hero.html#lua_api_hero_teleport", null ],
        [ "hero:get_direction()", "lua_api_hero.html#lua_api_hero_get_direction", null ],
        [ "hero:set_direction(direction4)", "lua_api_hero.html#lua_api_hero_set_direction", null ],
        [ "hero:get_walking_speed()", "lua_api_hero.html#lua_api_hero_get_walking_speed", null ],
        [ "hero:set_walking_speed(walking_speed)", "lua_api_hero.html#lua_api_hero_set_walking_speed", null ],
        [ "hero:save_solid_ground([x, y, layer]), hero:save_solid_ground(callback)", "lua_api_hero.html#lua_api_hero_save_solid_ground", null ],
        [ "hero:reset_solid_ground()", "lua_api_hero.html#lua_api_hero_reset_solid_ground", null ],
        [ "hero:get_solid_ground_position()", "lua_api_hero.html#lua_api_hero_get_solid_ground_position", null ],
        [ "hero:get_animation()", "lua_api_hero.html#lua_api_hero_get_animation", null ],
        [ "hero:set_animation(animation, [callback])", "lua_api_hero.html#lua_api_hero_set_animation", null ],
        [ "hero:get_tunic_sprite_id()", "lua_api_hero.html#lua_api_hero_get_tunic_sprite_id", null ],
        [ "hero:set_tunic_sprite_id(sprite_id)", "lua_api_hero.html#lua_api_hero_set_tunic_sprite_id", null ],
        [ "hero:get_sword_sprite_id()", "lua_api_hero.html#lua_api_hero_get_sword_sprite_id", null ],
        [ "hero:set_sword_sprite_id(sprite_id)", "lua_api_hero.html#lua_api_hero_set_sword_sprite_id", null ],
        [ "hero:get_sword_sound_id()", "lua_api_hero.html#lua_api_hero_get_sword_sound_id", null ],
        [ "hero:set_sword_sound_id(sound_id)", "lua_api_hero.html#lua_api_hero_set_sword_sound_id", null ],
        [ "hero:get_shield_sprite_id()", "lua_api_hero.html#lua_api_hero_get_shield_sprite_id", null ],
        [ "hero:set_shield_sprite_id(sprite_id)", "lua_api_hero.html#lua_api_hero_set_shield_sprite_id", null ],
        [ "hero:is_invincible()", "lua_api_hero.html#lua_api_hero_is_invincible", null ],
        [ "hero:set_invincible([invincible, [duration]])", "lua_api_hero.html#lua_api_hero_set_invincible", null ],
        [ "hero:is_blinking()", "lua_api_hero.html#lua_api_hero_is_blinking", null ],
        [ "hero:set_blinking([blinking, [duration]])", "lua_api_hero.html#lua_api_hero_set_blinking", null ],
        [ "hero:get_carried_object()", "lua_api_hero.html#lua_api_hero_get_carried_object", null ],
        [ "hero:freeze()", "lua_api_hero.html#lua_api_hero_freeze", null ],
        [ "hero:unfreeze()", "lua_api_hero.html#lua_api_hero_unfreeze", null ],
        [ "hero:walk(path, [loop, [ignore_obstacles]])", "lua_api_hero.html#lua_api_hero_walk", null ],
        [ "hero:start_jumping(direction8, distance, [ignore_obstacles])", "lua_api_hero.html#lua_api_hero_start_jumping", null ],
        [ "hero:start_attack()", "lua_api_hero.html#lua_api_hero_start_attack", null ],
        [ "hero:start_attack_loading([spin_attack_delay])", "lua_api_hero.html#lua_api_hero_start_attack_loading", null ],
        [ "hero:start_item(item)", "lua_api_hero.html#lua_api_hero_start_item", null ],
        [ "hero:start_grabbing()", "lua_api_hero.html#lua_api_hero_start_grabbing", null ],
        [ "hero:start_treasure(treasure_name, [treasure_variant, [treasure_savegame_variable, [callback]]])", "lua_api_hero.html#lua_api_hero_start_treasure", null ],
        [ "hero:start_victory([callback])", "lua_api_hero.html#lua_api_hero_start_victory", null ],
        [ "hero:start_boomerang(max_distance, speed, tunic_preparing_animation, sprite_name)", "lua_api_hero.html#lua_api_hero_start_boomerang", null ],
        [ "hero:start_bow()", "lua_api_hero.html#lua_api_hero_start_bow", null ],
        [ "hero:start_hookshot()", "lua_api_hero.html#lua_api_hero_start_hookshot", null ],
        [ "hero:start_running()", "lua_api_hero.html#lua_api_hero_start_running", null ],
        [ "hero:start_hurt(source_x, source_y, damage)", "lua_api_hero.html#lua_api_hero_start_hurt", null ],
        [ "hero:start_hurt([source_entity, [source_sprite]], damage)", "lua_api_hero.html#lua_api_hero_start_hurt_entity", null ],
        [ "hero:get_state()", "lua_api_hero.html#lua_api_hero_get_state", null ],
        [ "hero:start_state(state)", "lua_api_hero.html#lua_api_hero_start_state", null ],
        [ "hero:get_state_object()", "lua_api_hero.html#lua_api_hero_get_state_object", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_hero.html#lua_api_hero_inherited_events", null ],
      [ "Events of the type hero", "lua_api_hero.html#lua_api_hero_events", [
        [ "hero:on_state_changing(state_name, next_state_name)", "lua_api_hero.html#lua_api_hero_on_state_changing", null ],
        [ "hero:on_state_changed(new_state_name)", "lua_api_hero.html#lua_api_hero_on_state_changed", null ],
        [ "hero:on_taking_damage(damage)", "lua_api_hero.html#lua_api_hero_on_taking_damage", null ]
      ] ]
    ] ],
    [ "Tile", "lua_api_tile.html", [
      [ "Overview", "lua_api_tile.html#lua_api_tile_overview", [
        [ "Tiles are designed for performance", "lua_api_tile.html#lua_api_tile_overview_performance", null ]
      ] ]
    ] ],
    [ "Dynamic tile", "lua_api_dynamic_tile.html", [
      [ "Overview", "lua_api_dynamic_tile.html#lua_api_dynamic_tile_overview", null ],
      [ "Methods inherited from map entity", "lua_api_dynamic_tile.html#lua_api_dynamic_tile_inherited_methods", null ],
      [ "Methods of the type dynamic tile", "lua_api_dynamic_tile.html#lua_api_dynamic_tile_methods", [
        [ "dynamic_tile:get_pattern_id()", "lua_api_dynamic_tile.html#lua_api_dynamic_tile_get_pattern_id", null ],
        [ "dynamic_tile:get_tileset()", "lua_api_dynamic_tile.html#lua_api_dynamic_tile_get_tileset", null ],
        [ "dynamic_tile:set_tileset(tileset_id)", "lua_api_dynamic_tile.html#lua_api_dynamic_tile_set_tileset", null ],
        [ "dynamic_tile:get_modified_ground()", "lua_api_dynamic_tile.html#lua_api_dynamic_tile_get_modified_ground", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_dynamic_tile.html#lua_api_dynamic_tile_inherited_events", null ],
      [ "Events of the type dynamic tile", "lua_api_dynamic_tile.html#lua_api_dynamic_tile_events", null ]
    ] ],
    [ "Teletransporter", "lua_api_teletransporter.html", [
      [ "Overview", "lua_api_teletransporter.html#lua_api_teletransporter_overview", null ],
      [ "Methods inherited from map entity", "lua_api_teletransporter.html#lua_api_teletransporter_inherited_methods", null ],
      [ "Methods of the type teletransporter", "lua_api_teletransporter.html#lua_api_teletransporter_methods", [
        [ "teletransporter:get_sound()", "lua_api_teletransporter.html#lua_api_teletransporter_get_sound", null ],
        [ "teletransporter:set_sound(sound_id)", "lua_api_teletransporter.html#lua_api_teletransporter_set_sound", null ],
        [ "teletransporter:get_transition()", "lua_api_teletransporter.html#lua_api_teletransporter_get_transition", null ],
        [ "teletransporter:set_transition(transition_style)", "lua_api_teletransporter.html#lua_api_teletransporter_set_transition", null ],
        [ "teletransporter:get_destination_map()", "lua_api_teletransporter.html#lua_api_teletransporter_get_destination_map", null ],
        [ "teletransporter:set_destination_map(map_id)", "lua_api_teletransporter.html#lua_api_teletransporter_set_destination_map", null ],
        [ "teletransporter:get_destination_name()", "lua_api_teletransporter.html#lua_api_teletransporter_get_destination_name", null ],
        [ "teletransporter:set_destination_name(destination_name)", "lua_api_teletransporter.html#lua_api_teletransporter_set_destination_name", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_teletransporter.html#lua_api_teletransporter_inherited_events", null ],
      [ "Events of the type teletransporter", "lua_api_teletransporter.html#lua_api_teletransporter_events", [
        [ "teletransporter:on_activated()", "lua_api_teletransporter.html#lua_api_teletransporter_on_activated", null ]
      ] ]
    ] ],
    [ "Destination", "lua_api_destination.html", [
      [ "Overview", "lua_api_destination.html#lua_api_destination_overview", null ],
      [ "Methods inherited from map entity", "lua_api_destination.html#lua_api_destination_inherited_methods", null ],
      [ "Methods of the type destination", "lua_api_destination.html#lua_api_destination_methods", [
        [ "destination:get_starting_location_mode()", "lua_api_destination.html#lua_api_destination_get_starting_location_mode", null ],
        [ "destination:set_starting_location_mode(mode)", "lua_api_destination.html#lua_api_destination_set_starting_location_mode", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_destination.html#lua_api_destination_inherited_events", null ],
      [ "Events of the type destination", "lua_api_destination.html#lua_api_destination_events", [
        [ "destination:on_activated()", "lua_api_destination.html#lua_api_destination_on_activated", null ]
      ] ]
    ] ],
    [ "Pickable treasure", "lua_api_pickable.html", [
      [ "Overview", "lua_api_pickable.html#lua_api_pickable_overview", [
        [ "Pickable treasure sprites", "lua_api_pickable.html#lua_api_pickable_overview_sprites", null ]
      ] ],
      [ "Methods inherited from map entity", "lua_api_pickable.html#lua_api_pickable_inherited_methods", null ],
      [ "Methods of the type pickable", "lua_api_pickable.html#lua_api_pickable_methods", [
        [ "pickable:has_layer_independent_collisions()", "lua_api_pickable.html#lua_api_pickable_has_layer_independent_collisions", null ],
        [ "pickable:set_layer_independent_collisions([independent])", "lua_api_pickable.html#lua_api_pickable_set_layer_independent_collisions", null ],
        [ "pickable:get_followed_entity()", "lua_api_pickable.html#lua_api_pickable_get_followed_entity", null ],
        [ "pickable:get_falling_height()", "lua_api_pickable.html#lua_api_pickable_get_falling_height", null ],
        [ "pickable:get_treasure()", "lua_api_pickable.html#lua_api_pickable_get_treasure", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_pickable.html#lua_api_pickable_inherited_events", null ],
      [ "Events of the type pickable", "lua_api_pickable.html#lua_api_pickable_events", null ]
    ] ],
    [ "Destructible object", "lua_api_destructible.html", [
      [ "Overview", "lua_api_destructible.html#lua_api_destructible_overview", null ],
      [ "Methods inherited from map entity", "lua_api_destructible.html#lua_api_destructible_inherited_methods", null ],
      [ "Methods of the type destructible", "lua_api_destructible.html#lua_api_destructible_methods", [
        [ "destructible:get_treasure()", "lua_api_destructible.html#lua_api_destructible_get_treasure", null ],
        [ "destructible:set_treasure([item_name, [variant, [savegame_variable]]])", "lua_api_destructible.html#lua_api_destructible_set_treasure", null ],
        [ "destructible:get_destruction_sound()", "lua_api_destructible.html#lua_api_destructible_get_destruction_sound", null ],
        [ "destructible:set_destruction_sound(destruction_sound_id)", "lua_api_destructible.html#lua_api_destructible_set_destruction_sound", null ],
        [ "destructible:get_can_be_cut()", "lua_api_destructible.html#lua_api_destructible_get_can_be_cut", null ],
        [ "destructible:set_can_be_cut(can_be_cut)", "lua_api_destructible.html#lua_api_destructible_set_can_be_cut", null ],
        [ "destructible:get_cut_method()", "lua_api_destructible.html#lua_api_destructible_get_cut_method", null ],
        [ "destructible:set_cut_method(cut_method)", "lua_api_destructible.html#lua_api_destructible_set_cut_method", null ],
        [ "destructible:get_can_explode()", "lua_api_destructible.html#lua_api_destructible_get_can_explode", null ],
        [ "destructible:set_can_explode(can_explode)", "lua_api_destructible.html#lua_api_destructible_set_can_explode", null ],
        [ "destructible:get_can_regenerate()", "lua_api_destructible.html#lua_api_destructible_get_can_regenerate", null ],
        [ "destructible:set_can_regenerate(can_regenerate)", "lua_api_destructible.html#lua_api_destructible_set_can_regenerate", null ],
        [ "destructible:get_damage_on_enemies()", "lua_api_destructible.html#lua_api_destructible_get_damage_on_enemies", null ],
        [ "destructible:set_damage_on_enemies(damage_on_enemies)", "lua_api_destructible.html#lua_api_destructible_set_damage_on_enemies", null ],
        [ "destructible:get_modified_ground()", "lua_api_destructible.html#lua_api_destructible_get_modified_ground", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_destructible.html#lua_api_destructible_inherited_events", null ],
      [ "Events of the type destructible", "lua_api_destructible.html#lua_api_destructible_events", [
        [ "destructible:on_looked()", "lua_api_destructible.html#lua_api_destructible_on_looked", null ],
        [ "destructible:on_cut()", "lua_api_destructible.html#lua_api_destructible_on_cut", null ],
        [ "destructible:on_exploded()", "lua_api_destructible.html#lua_api_destructible_on_exploded", null ],
        [ "destructible:on_regenerating()", "lua_api_destructible.html#lua_api_destructible_on_regenerating", null ]
      ] ]
    ] ],
    [ "Carried object", "lua_api_carried_object.html", [
      [ "Overview", "lua_api_carried_object.html#lua_api_carried_object_overview", [
        [ "Carried object sprites", "lua_api_carried_object.html#lua_api_carried_object_overview_sprites", null ]
      ] ],
      [ "Methods inherited from map entity", "lua_api_carried_object.html#lua_api_carried_object_inherited_methods", null ],
      [ "Methods of the type carried object", "lua_api_carried_object.html#lua_api_carried_object_methods", [
        [ "carried_object:get_carrier()", "lua_api_carried_object.html#lua_api_carried_object_get_carrier", null ],
        [ "carried_object:get_damage_on_enemies()", "lua_api_carried_object.html#lua_api_carried_object_get_damage_on_enemies", null ],
        [ "carried_object:set_damage_on_enemies(damage_on_enemies)", "lua_api_carried_object.html#lua_api_carried_object_set_damage_on_enemies", null ],
        [ "carried_object:get_destruction_sound()", "lua_api_carried_object.html#lua_api_carried_object_get_destruction_sound", null ],
        [ "carried_object:set_destruction_sound(destruction_sound_id)", "lua_api_carried_object.html#lua_api_carried_object_set_destruction_sound", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_carried_object.html#lua_api_carried_object_inherited_events", null ],
      [ "Events of the type carried object", "lua_api_carried_object.html#lua_api_carried_object_events", [
        [ "carried_object:on_lifted()", "lua_api_carried_object.html#lua_api_carried_object_on_lifted", null ],
        [ "carried_object:on_thrown()", "lua_api_carried_object.html#lua_api_carried_object_on_thrown", null ],
        [ "carried_object:on_breaking()", "lua_api_carried_object.html#lua_api_carried_object_on_breaking", null ]
      ] ]
    ] ],
    [ "Chest", "lua_api_chest.html", [
      [ "Overview", "lua_api_chest.html#lua_api_chest_overview", null ],
      [ "Methods inherited from map entity", "lua_api_chest.html#lua_api_chest_inherited_methods", null ],
      [ "Methods of the type chest", "lua_api_chest.html#lua_api_chest_methods", [
        [ "chest:is_open()", "lua_api_chest.html#lua_api_chest_is_open", null ],
        [ "chest:set_open([open])", "lua_api_chest.html#lua_api_chest_set_open", null ],
        [ "chest:get_treasure()", "lua_api_chest.html#lua_api_chest_get_treasure", null ],
        [ "chest:set_treasure([item_name, [variant, [savegame_variable]]])", "lua_api_chest.html#lua_api_chest_set_treasure", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_chest.html#lua_api_chest_inherited_events", null ],
      [ "Events of the type chest", "lua_api_chest.html#lua_api_chest_events", [
        [ "chest:on_opened(treasure_item, treasure_variant, treasure_savegame_variable)", "lua_api_chest.html#lua_api_chest_on_opened", null ]
      ] ]
    ] ],
    [ "Shop treasure", "lua_api_shop_treasure.html", [
      [ "Overview", "lua_api_shop_treasure.html#lua_api_shop_treasure_overview", null ],
      [ "Dialogs of shop treasures", "lua_api_shop_treasure.html#lua_api_shop_treasure_dialogs", null ],
      [ "Methods inherited from map entity", "lua_api_shop_treasure.html#lua_api_shop_treasure_inherited_methods", null ],
      [ "Methods of the type shop treasure", "lua_api_shop_treasure.html#lua_api_shop_treasure_methods", null ],
      [ "Events inherited from map entity", "lua_api_shop_treasure.html#lua_api_shop_treasure_inherited_events", null ],
      [ "Events of the type shop treasure", "lua_api_shop_treasure.html#lua_api_shop_treasure_events", [
        [ "shop_treasure:on_buying()", "lua_api_shop_treasure.html#lua_api_shop_treasure_on_buying", null ],
        [ "shop_treasure:on_bought()", "lua_api_shop_treasure.html#lua_api_shop_treasure_on_bought", null ]
      ] ]
    ] ],
    [ "Enemy", "lua_api_enemy.html", [
      [ "Overview", "lua_api_enemy.html#lua_api_enemy_overview", null ],
      [ "Methods inherited from map entity", "lua_api_enemy.html#lua_api_enemy_inherited_methods", null ],
      [ "Methods of the type enemy", "lua_api_enemy.html#lua_api_enemy_methods", [
        [ "enemy:get_breed()", "lua_api_enemy.html#lua_api_enemy_get_breed", null ],
        [ "enemy:get_life()", "lua_api_enemy.html#lua_api_enemy_get_life", null ],
        [ "enemy:set_life(life)", "lua_api_enemy.html#lua_api_enemy_set_life", null ],
        [ "enemy:add_life(life)", "lua_api_enemy.html#lua_api_enemy_add_life", null ],
        [ "enemy:remove_life(life)", "lua_api_enemy.html#lua_api_enemy_remove_life", null ],
        [ "enemy:get_damage()", "lua_api_enemy.html#lua_api_enemy_get_damage", null ],
        [ "enemy:set_damage(damage)", "lua_api_enemy.html#lua_api_enemy_set_damage", null ],
        [ "enemy:is_pushed_back_when_hurt()", "lua_api_enemy.html#lua_api_enemy_is_pushed_back_when_hurt", null ],
        [ "enemy:set_pushed_back_when_hurt([pushed_back_when_hurt])", "lua_api_enemy.html#lua_api_enemy_set_pushed_back_when_hurt", null ],
        [ "enemy:get_push_hero_on_sword()", "lua_api_enemy.html#lua_api_enemy_get_push_hero_on_sword", null ],
        [ "enemy:set_push_hero_on_sword([push_hero_on_sword])", "lua_api_enemy.html#lua_api_enemy_set_push_hero_on_sword", null ],
        [ "enemy:get_can_hurt_hero_running()", "lua_api_enemy.html#lua_api_enemy_get_can_hurt_hero_running", null ],
        [ "enemy:set_can_hurt_hero_running([can_hurt_hero_running])", "lua_api_enemy.html#lua_api_enemy_set_can_hurt_hero_running", null ],
        [ "enemy:get_hurt_style()", "lua_api_enemy.html#lua_api_enemy_get_hurt_style", null ],
        [ "enemy:set_hurt_style(hurt_style)", "lua_api_enemy.html#lua_api_enemy_set_hurt_style", null ],
        [ "enemy:get_dying_sprite_id()", "lua_api_enemy.html#lua_api_enemy_get_dying_sprite_id", null ],
        [ "enemy:set_dying_sprite_id(dying_sprite_id)", "lua_api_enemy.html#lua_api_enemy_set_dying_sprite_id", null ],
        [ "enemy:get_can_attack()", "lua_api_enemy.html#lua_api_enemy_get_can_attack", null ],
        [ "enemy:set_can_attack([can_attack])", "lua_api_enemy.html#lua_api_enemy_set_can_attack", null ],
        [ "enemy:get_minimum_shield_needed()", "lua_api_enemy.html#lua_api_enemy_get_minimum_shield_needed", null ],
        [ "enemy:set_minimum_shield_needed(minimum_shield_needed)", "lua_api_enemy.html#lua_api_enemy_set_minimum_shield_needed", null ],
        [ "enemy:is_traversable()", "lua_api_enemy.html#lua_api_enemy_is_traversable", null ],
        [ "enemy:set_traversable([traversable])", "lua_api_enemy.html#lua_api_enemy_set_traversable", null ],
        [ "enemy:get_attacking_collision_mode()", "lua_api_enemy.html#lua_api_enemy_get_attacking_collision_mode", null ],
        [ "enemy:set_attacking_collision_mode(collision_mode)", "lua_api_enemy.html#lua_api_enemy_set_attacking_collision_mode", null ],
        [ "enemy:get_attack_consequence(attack)", "lua_api_enemy.html#lua_api_enemy_get_attack_consequence", null ],
        [ "enemy:set_attack_consequence(attack, consequence)", "lua_api_enemy.html#lua_api_enemy_set_attack_consequence", null ],
        [ "enemy:get_attack_consequence_sprite(sprite, attack)", "lua_api_enemy.html#lua_api_enemy_get_attack_consequence_sprite", null ],
        [ "enemy:set_attack_consequence_sprite(sprite, attack, consequence)", "lua_api_enemy.html#lua_api_enemy_set_attack_consequence_sprite", null ],
        [ "enemy:set_default_attack_consequences()", "lua_api_enemy.html#lua_api_enemy_set_default_attack_consequences", null ],
        [ "enemy:set_default_attack_consequences_sprite(sprite)", "lua_api_enemy.html#lua_api_enemy_set_default_attack_consequences_sprite", null ],
        [ "enemy:set_invincible()", "lua_api_enemy.html#lua_api_enemy_set_invincible", null ],
        [ "enemy:set_invincible_sprite(sprite)", "lua_api_enemy.html#lua_api_enemy_set_invincible_sprite", null ],
        [ "enemy:has_layer_independent_collisions()", "lua_api_enemy.html#lua_api_enemy_has_layer_independent_collisions", null ],
        [ "enemy:set_layer_independent_collisions([independent])", "lua_api_enemy.html#lua_api_enemy_set_layer_independent_collisions", null ],
        [ "enemy:get_treasure()", "lua_api_enemy.html#lua_api_enemy_get_treasure", null ],
        [ "enemy:set_treasure([item_name, [variant, [savegame_variable]]])", "lua_api_enemy.html#lua_api_enemy_set_treasure", null ],
        [ "enemy:get_obstacle_behavior()", "lua_api_enemy.html#lua_api_enemy_get_obstacle_behavior", null ],
        [ "enemy:set_obstacle_behavior(obstacle_behavior)", "lua_api_enemy.html#lua_api_enemy_set_obstacle_behavior", null ],
        [ "enemy:restart()", "lua_api_enemy.html#lua_api_enemy_restart", null ],
        [ "enemy:hurt(life_points)", "lua_api_enemy.html#lua_api_enemy_hurt", null ],
        [ "enemy:is_immobilized()", "lua_api_enemy.html#lua_api_enemy_is_immobilized", null ],
        [ "enemy:immobilize()", "lua_api_enemy.html#lua_api_enemy_immobilize", null ],
        [ "enemy:create_enemy(properties)", "lua_api_enemy.html#lua_api_enemy_create_enemy", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_enemy.html#lua_api_enemy_inherited_events", null ],
      [ "Events of the type enemy", "lua_api_enemy.html#lua_api_enemy_events", [
        [ "enemy:on_update()", "lua_api_enemy.html#lua_api_enemy_on_update", null ],
        [ "enemy:on_restarted()", "lua_api_enemy.html#lua_api_enemy_on_restarted", null ],
        [ "enemy:on_collision_enemy(other_enemy, other_sprite, my_sprite)", "lua_api_enemy.html#lua_api_enemy_on_collision_enemy", null ],
        [ "enemy:on_custom_attack_received(attack, sprite)", "lua_api_enemy.html#lua_api_enemy_on_custom_attack_received", null ],
        [ "enemy:on_hurt_by_sword(hero, enemy_sprite)", "lua_api_enemy.html#lua_api_enemy_on_hurt_by_sword", null ],
        [ "enemy:on_hurt(attack)", "lua_api_enemy.html#lua_api_enemy_on_hurt", null ],
        [ "enemy:on_dying()", "lua_api_enemy.html#lua_api_enemy_on_dying", null ],
        [ "enemy:on_dead()", "lua_api_enemy.html#lua_api_enemy_on_dead", null ],
        [ "enemy:on_immobilized()", "lua_api_enemy.html#lua_api_enemy_on_immobilized", null ],
        [ "enemy:on_attacking_hero(hero, enemy_sprite)", "lua_api_enemy.html#lua_api_enemy_on_attacking_hero", null ]
      ] ]
    ] ],
    [ "Non-playing character", "lua_api_npc.html", [
      [ "Overview", "lua_api_npc.html#lua_api_npc_overview", null ],
      [ "Methods inherited from map entity", "lua_api_npc.html#lua_api_npc_inherited_methods", null ],
      [ "Methods of the type non-playing character", "lua_api_npc.html#lua_api_npc_methods", [
        [ "npc:is_traversable()", "lua_api_npc.html#lua_api_npc_is_traversable", null ],
        [ "npc:set_traversable([traversable])", "lua_api_npc.html#lua_api_npc_set_traversable", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_npc.html#lua_api_npc_inherited_events", null ],
      [ "Events of the type non-playing character", "lua_api_npc.html#lua_api_npc_events", [
        [ "npc:on_interaction()", "lua_api_npc.html#lua_api_npc_on_interaction", null ],
        [ "npc:on_interaction_item(item_used)", "lua_api_npc.html#lua_api_npc_on_interaction_item", null ],
        [ "npc:on_collision_fire()", "lua_api_npc.html#lua_api_npc_on_collision_fire", null ]
      ] ]
    ] ],
    [ "Block", "lua_api_block.html", [
      [ "Overview", "lua_api_block.html#lua_api_block_overview", null ],
      [ "Methods inherited from map entity", "lua_api_block.html#lua_api_block_inherited_methods", null ],
      [ "Methods of the type block", "lua_api_block.html#lua_api_block_methods", [
        [ "block:reset()", "lua_api_block.html#lua_api_block_reset", null ],
        [ "block:is_pushable()", "lua_api_block.html#lua_api_block_is_pushable", null ],
        [ "block:set_pushable([pushable])", "lua_api_block.html#lua_api_block_set_pushable", null ],
        [ "block:is_pullable()", "lua_api_block.html#lua_api_block_is_pullable", null ],
        [ "block:set_pullable([pullable])", "lua_api_block.html#lua_api_block_set_pullable", null ],
        [ "block:get_max_moves()", "lua_api_block.html#lua_api_block_get_max_moves", null ],
        [ "block:set_max_moves(max_moves)", "lua_api_block.html#lua_api_block_set_max_moves", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_block.html#lua_api_block_inherited_events", null ],
      [ "Events of the type block", "lua_api_block.html#lua_api_block_events", [
        [ "block:on_moving()", "lua_api_block.html#lua_api_block_on_moving", null ],
        [ "block:on_moved()", "lua_api_block.html#lua_api_block_on_moved", null ]
      ] ],
      [ "Deprecated methods of the type block", "lua_api_block.html#lua_api_block_methods_deprecated", [
        [ "block:get_maximum_moves()", "lua_api_block.html#lua_api_block_get_maximum_moves", null ],
        [ "block:set_maximum_moves(maximum_moves)", "lua_api_block.html#lua_api_block_set_maximum_moves", null ]
      ] ]
    ] ],
    [ "Jumper", "lua_api_jumper.html", [
      [ "Overview", "lua_api_jumper.html#lua_api_jumper_overview", null ],
      [ "Methods inherited from map entity", "lua_api_jumper.html#lua_api_jumper_inherited_methods", null ],
      [ "Methods of the type jumper", "lua_api_jumper.html#lua_api_jumper_methods", null ],
      [ "Events inherited from map entity", "lua_api_jumper.html#lua_api_jumper_inherited_events", null ],
      [ "Events of the type jumper", "lua_api_jumper.html#lua_api_jumper_events", null ]
    ] ],
    [ "Switch", "lua_api_switch.html", [
      [ "Overview", "lua_api_switch.html#lua_api_switch_overview", null ],
      [ "Methods inherited from map entity", "lua_api_switch.html#lua_api_switch_inherited_methods", null ],
      [ "Methods of the type switch", "lua_api_switch.html#lua_api_switch_methods", [
        [ "switch:is_walkable()", "lua_api_switch.html#lua_api_switch_is_walkable", null ],
        [ "switch:is_activated()", "lua_api_switch.html#lua_api_switch_is_activated", null ],
        [ "switch:set_activated([activated])", "lua_api_switch.html#lua_api_switch_set_activated", null ],
        [ "switch:is_locked()", "lua_api_switch.html#lua_api_switch_is_locked", null ],
        [ "switch:set_locked([locked])", "lua_api_switch.html#lua_api_switch_set_locked", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_switch.html#lua_api_switch_inherited_events", null ],
      [ "Events of the type switch", "lua_api_switch.html#lua_api_switch_events", [
        [ "switch:on_activated()", "lua_api_switch.html#lua_api_switch_on_activated", null ],
        [ "switch:on_inactivated()", "lua_api_switch.html#lua_api_switch_on_inactivated", null ],
        [ "switch:on_left()", "lua_api_switch.html#lua_api_switch_on_left", null ]
      ] ]
    ] ],
    [ "Sensor", "lua_api_sensor.html", [
      [ "Overview", "lua_api_sensor.html#lua_api_sensor_overview", null ],
      [ "Methods inherited from map entity", "lua_api_sensor.html#lua_api_sensor_inherited_methods", null ],
      [ "Methods of the type sensor", "lua_api_sensor.html#lua_api_sensor_methods", null ],
      [ "Events inherited from map entity", "lua_api_sensor.html#lua_api_sensor_inherited_events", null ],
      [ "Events of the type sensor", "lua_api_sensor.html#lua_api_sensor_events", [
        [ "sensor:on_activated()", "lua_api_sensor.html#lua_api_sensor_on_activated", null ],
        [ "sensor:on_activated_repeat()", "lua_api_sensor.html#lua_api_sensor_on_activated_repeat", null ],
        [ "sensor:on_left()", "lua_api_sensor.html#lua_api_sensor_on_left", null ],
        [ "sensor:on_collision_explosion()", "lua_api_sensor.html#lua_api_sensor_on_collision_explosion", null ]
      ] ]
    ] ],
    [ "Separator", "lua_api_separator.html", [
      [ "Overview", "lua_api_separator.html#lua_api_separator_overview", null ],
      [ "Methods inherited from map entity", "lua_api_separator.html#lua_api_separator_inherited_methods", null ],
      [ "Methods of the type separator", "lua_api_separator.html#lua_api_separator_methods", null ],
      [ "Events inherited from map entity", "lua_api_separator.html#lua_api_separator_inherited_events", null ],
      [ "Events of the type separator", "lua_api_separator.html#lua_api_separator_events", [
        [ "separator:on_activating(direction4)", "lua_api_separator.html#lua_api_separator_on_activating", null ],
        [ "separator:on_activated(direction4)", "lua_api_separator.html#lua_api_separator_on_activated", null ]
      ] ]
    ] ],
    [ "Wall", "lua_api_wall.html", [
      [ "Overview", "lua_api_wall.html#lua_api_wall_overview", null ],
      [ "Methods inherited from map entity", "lua_api_wall.html#lua_api_wall_inherited_methods", null ],
      [ "Methods of the type wall", "lua_api_wall.html#lua_api_wall_methods", null ],
      [ "Events inherited from map entity", "lua_api_wall.html#lua_api_wall_inherited_events", null ],
      [ "Events of the type wall", "lua_api_wall.html#lua_api_wall_events", null ]
    ] ],
    [ "Crystal", "lua_api_crystal.html", [
      [ "Overview", "lua_api_crystal.html#lua_api_crystal_overview", [
        [ "Crystal sprites", "lua_api_crystal.html#lua_api_crystal_overview_sprites", null ]
      ] ],
      [ "Methods inherited from map entity", "lua_api_crystal.html#lua_api_crystal_inherited_methods", null ],
      [ "Methods of the type crystal", "lua_api_crystal.html#lua_api_crystal_methods", null ],
      [ "Events inherited from map entity", "lua_api_crystal.html#lua_api_crystal_inherited_events", null ],
      [ "Events of the type crystal", "lua_api_crystal.html#lua_api_crystal_events", null ]
    ] ],
    [ "Crystal block", "lua_api_crystal_block.html", [
      [ "Overview", "lua_api_crystal_block.html#lua_api_crystal_block_overview", null ],
      [ "Methods inherited from map entity", "lua_api_crystal_block.html#lua_api_crystal_block_inherited_methods", null ],
      [ "Methods of the type crystal", "lua_api_crystal_block.html#lua_api_crystal_block_methods", null ],
      [ "Events inherited from map entity", "lua_api_crystal_block.html#lua_api_crystal_block_inherited_events", null ],
      [ "Events of the type crystal", "lua_api_crystal_block.html#lua_api_crystal_block_events", null ]
    ] ],
    [ "Stream", "lua_api_stream.html", [
      [ "Overview", "lua_api_stream.html#lua_api_stream_overview", null ],
      [ "Streams and holes", "lua_api_stream.html#lua_api_stream_holes", null ],
      [ "Methods inherited from map entity", "lua_api_stream.html#lua_api_stream_inherited_methods", null ],
      [ "Methods of the type stream", "lua_api_stream.html#lua_api_stream_methods", [
        [ "stream:get_direction()", "lua_api_stream.html#lua_api_stream_get_direction", null ],
        [ "stream:set_direction(direction)", "lua_api_stream.html#lua_api_stream_set_direction", null ],
        [ "stream:get_speed()", "lua_api_stream.html#lua_api_stream_get_speed", null ],
        [ "stream:set_speed(speed)", "lua_api_stream.html#lua_api_stream_set_speed", null ],
        [ "stream:get_allow_movement()", "lua_api_stream.html#lua_api_stream_get_allow_movement", null ],
        [ "stream:set_allow_movement(allow_movement)", "lua_api_stream.html#lua_api_stream_set_allow_movement", null ],
        [ "stream:get_allow_attack()", "lua_api_stream.html#lua_api_stream_get_allow_attack", null ],
        [ "stream:set_allow_attack(allow_attack)", "lua_api_stream.html#lua_api_stream_set_allow_attack", null ],
        [ "stream:get_allow_item()", "lua_api_stream.html#lua_api_stream_get_allow_item", null ],
        [ "stream:set_allow_item(allow_item)", "lua_api_stream.html#lua_api_stream_set_allow_item", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_stream.html#lua_api_stream_inherited_events", null ],
      [ "Events of the type stream", "lua_api_stream.html#lua_api_stream_events", null ]
    ] ],
    [ "Door", "lua_api_door.html", [
      [ "Overview", "lua_api_door.html#lua_api_door_overview", null ],
      [ "Methods inherited from map entity", "lua_api_door.html#lua_api_door_inherited_methods", null ],
      [ "Methods of the type door", "lua_api_door.html#lua_api_door_methods", [
        [ "door:is_open()", "lua_api_door.html#lua_api_door_is_open", null ],
        [ "door:is_opening()", "lua_api_door.html#lua_api_door_is_opening", null ],
        [ "door:is_closed()", "lua_api_door.html#lua_api_door_is_closed", null ],
        [ "door:is_closing()", "lua_api_door.html#lua_api_door_is_closing", null ],
        [ "door:open()", "lua_api_door.html#lua_api_door_open", null ],
        [ "door:close()", "lua_api_door.html#lua_api_door_close", null ],
        [ "door:set_open([open])", "lua_api_door.html#lua_api_door_set_open", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_door.html#lua_api_door_inherited_events", null ],
      [ "Events of the type door", "lua_api_door.html#lua_api_door_events", [
        [ "door:on_opened()", "lua_api_door.html#lua_api_door_on_opened", null ],
        [ "door:on_closed()", "lua_api_door.html#lua_api_door_on_closed", null ]
      ] ]
    ] ],
    [ "Stairs", "lua_api_stairs.html", [
      [ "Overview", "lua_api_stairs.html#lua_api_stairs_overview", null ],
      [ "Methods inherited from map entity", "lua_api_stairs.html#lua_api_stairs_inherited_methods", null ],
      [ "Methods of the type stairs", "lua_api_stairs.html#lua_api_stairs_methods", [
        [ "stairs:get_direction()", "lua_api_stairs.html#lua_api_stairs_get_direction", null ],
        [ "stairs:is_inner()", "lua_api_stairs.html#lua_api_stairs_is_inner", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_stairs.html#lua_api_stairs_inherited_events", null ],
      [ "Events of the type stairs", "lua_api_stairs.html#lua_api_stairs_events", null ]
    ] ],
    [ "Bomb", "lua_api_bomb.html", [
      [ "Overview", "lua_api_bomb.html#lua_api_bomb_overview", null ],
      [ "Methods inherited from map entity", "lua_api_bomb.html#lua_api_bomb_inherited_methods", null ],
      [ "Methods of the type bomb", "lua_api_bomb.html#lua_api_bomb_methods", null ],
      [ "Events inherited from map entity", "lua_api_bomb.html#lua_api_bomb_inherited_events", null ],
      [ "Events of the type bomb", "lua_api_bomb.html#lua_api_bomb_events", null ]
    ] ],
    [ "Explosion", "lua_api_explosion.html", [
      [ "Overview", "lua_api_explosion.html#lua_api_explosion_overview", null ],
      [ "Methods inherited from map entity", "lua_api_explosion.html#lua_api_explosion_inherited_methods", null ],
      [ "Methods of the type explosion", "lua_api_explosion.html#lua_api_explosion_methods", null ],
      [ "Events inherited from map entity", "lua_api_explosion.html#lua_api_explosion_inherited_events", null ],
      [ "Events of the type explosion", "lua_api_explosion.html#lua_api_explosion_events", null ]
    ] ],
    [ "Fire", "lua_api_fire.html", [
      [ "Overview", "lua_api_fire.html#lua_api_fire_overview", null ],
      [ "Methods inherited from map entity", "lua_api_fire.html#lua_api_fire_inherited_methods", null ],
      [ "Methods of the type fire", "lua_api_fire.html#lua_api_fire_methods", null ],
      [ "Events inherited from map entity", "lua_api_fire.html#lua_api_fire_inherited_events", null ],
      [ "Events of the type fire", "lua_api_fire.html#lua_api_fire_events", null ]
    ] ],
    [ "Arrow", "lua_api_arrow.html", [
      [ "Overview", "lua_api_arrow.html#lua_api_arrow_overview", null ],
      [ "Methods inherited from map entity", "lua_api_arrow.html#lua_api_arrow_inherited_methods", null ],
      [ "Methods of the type arrow", "lua_api_arrow.html#lua_api_arrow_methods", null ],
      [ "Events inherited from map entity", "lua_api_arrow.html#lua_api_arrow_inherited_events", null ],
      [ "Events of the type arrow", "lua_api_arrow.html#lua_api_arrow_events", null ]
    ] ],
    [ "Hookshot", "lua_api_hookshot.html", [
      [ "Overview", "lua_api_hookshot.html#lua_api_hookshot_overview", null ],
      [ "Methods inherited from map entity", "lua_api_hookshot.html#lua_api_hookshot_inherited_methods", null ],
      [ "Methods of the type hookshot", "lua_api_hookshot.html#lua_api_hookshot_methods", null ],
      [ "Events inherited from map entity", "lua_api_hookshot.html#lua_api_hookshot_inherited_events", null ],
      [ "Events of the type hookshot", "lua_api_hookshot.html#lua_api_hookshot_events", null ]
    ] ],
    [ "Boomerang", "lua_api_boomerang.html", [
      [ "Overview", "lua_api_boomerang.html#lua_api_boomerang_overview", null ],
      [ "Methods inherited from map entity", "lua_api_boomerang.html#lua_api_boomerang_inherited_methods", null ],
      [ "Methods of the type boomerang", "lua_api_boomerang.html#lua_api_boomerang_methods", null ],
      [ "Events inherited from map entity", "lua_api_boomerang.html#lua_api_boomerang_inherited_events", null ],
      [ "Events of the type boomerang", "lua_api_boomerang.html#lua_api_boomerang_events", null ]
    ] ],
    [ "Camera", "lua_api_camera.html", [
      [ "Overview", "lua_api_camera.html#lua_api_camera_overview", [
        [ "Movements and constraints", "lua_api_camera.html#lua_api_camera_constraints", null ],
        [ "Camera size", "lua_api_camera.html#lua_api_camera_overview_size", null ],
        [ "Suspending the game", "lua_api_camera.html#lua_api_camera_suspending", null ]
      ] ],
      [ "Methods inherited from map entity", "lua_api_camera.html#lua_api_camera_inherited_methods", null ],
      [ "Methods of the type camera", "lua_api_camera.html#lua_api_camera_methods", [
        [ "camera:get_position_on_screen()", "lua_api_camera.html#lua_api_camera_get_position_on_screen", null ],
        [ "camera:set_position_on_screen(x, y)", "lua_api_camera.html#lua_api_camera_set_position_on_screen", null ],
        [ "camera:get_position_to_track(entity), camera:get_position_to_track(x, y)", "lua_api_camera.html#lua_api_camera_get_position_to_track", null ],
        [ "camera:get_state()", "lua_api_camera.html#lua_api_camera_get_state", null ],
        [ "camera:start_tracking(entity)", "lua_api_camera.html#lua_api_camera_start_tracking", null ],
        [ "camera:get_tracked_entity()", "lua_api_camera.html#lua_api_camera_get_tracked_entity", null ],
        [ "camera:start_manual()", "lua_api_camera.html#lua_api_camera_start_manual", null ],
        [ "camera:get_surface()", "lua_api_camera.html#lua_api_camera_get_surface", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_camera.html#lua_api_camera_inherited_events", null ],
      [ "Events of the type camera", "lua_api_camera.html#lua_api_camera_events", [
        [ "camera:on_state_changing(state_name, next_state_name)", "lua_api_camera.html#lua_api_camera_on_state_changing", null ],
        [ "camera:on_state_changed(new_state_name)", "lua_api_camera.html#lua_api_camera_on_state_changed", null ]
      ] ]
    ] ],
    [ "Custom entity", "lua_api_custom_entity.html", [
      [ "Overview", "lua_api_custom_entity.html#lua_api_custom_entity_overview", null ],
      [ "Methods inherited from map entity", "lua_api_custom_entity.html#lua_api_custom_entity_inherited_methods", null ],
      [ "Methods of the type custom entity", "lua_api_custom_entity.html#lua_api_custom_entity_methods", [
        [ "custom_entity:get_model()", "lua_api_custom_entity.html#lua_api_custom_entity_get_model", null ],
        [ "custom_entity:get_direction()", "lua_api_custom_entity.html#lua_api_custom_entity_get_direction", null ],
        [ "custom_entity:set_direction(direction)", "lua_api_custom_entity.html#lua_api_custom_entity_set_direction", null ],
        [ "custom_entity:is_tiled()", "lua_api_custom_entity.html#lua_api_custom_entity_is_tiled", null ],
        [ "custom_entity:set_tiled([tiled])", "lua_api_custom_entity.html#lua_api_custom_entity_set_tiled", null ],
        [ "custom_entity:set_traversable_by([entity_type], traversable)", "lua_api_custom_entity.html#lua_api_custom_entity_set_traversable_by", null ],
        [ "custom_entity:set_can_traverse([entity_type], traversable)", "lua_api_custom_entity.html#lua_api_custom_entity_set_can_traverse", null ],
        [ "custom_entity:can_traverse_ground(ground)", "lua_api_custom_entity.html#lua_api_custom_entity_can_traverse_ground", null ],
        [ "custom_entity:set_can_traverse_ground(ground, traversable)", "lua_api_custom_entity.html#lua_api_custom_entity_set_can_traverse_ground", null ],
        [ "custom_entity:add_collision_test(collision_mode, callback)", "lua_api_custom_entity.html#lua_api_custom_entity_add_collision_test", null ],
        [ "custom_entity:clear_collision_tests()", "lua_api_custom_entity.html#lua_api_custom_entity_clear_collision_tests", null ],
        [ "custom_entity:has_layer_independent_collisions()", "lua_api_custom_entity.html#lua_api_custom_entity_has_layer_independent_collisions", null ],
        [ "custom_entity:set_layer_independent_collisions([independent])", "lua_api_custom_entity.html#lua_api_custom_entity_set_layer_independent_collisions", null ],
        [ "custom_entity:get_modified_ground()", "lua_api_custom_entity.html#lua_api_custom_entity_get_modified_ground", null ],
        [ "custom_entity:set_modified_ground(modified_ground)", "lua_api_custom_entity.html#lua_api_custom_entity_set_modified_ground", null ],
        [ "custom_entity:get_follow_streams()", "lua_api_custom_entity.html#lua_api_custom_entity_get_follow_streams", null ],
        [ "custom_entity:set_follow_streams([follow_streams])", "lua_api_custom_entity.html#lua_api_custom_entity_set_follow_streams", null ]
      ] ],
      [ "Events inherited from map entity", "lua_api_custom_entity.html#lua_api_custom_entity_inherited_events", null ],
      [ "Events of the type custom entity", "lua_api_custom_entity.html#lua_api_custom_entity_events", [
        [ "custom_entity:on_update()", "lua_api_custom_entity.html#lua_api_custom_entity_on_update", null ],
        [ "custom_entity:on_ground_below_changed(ground_below)", "lua_api_custom_entity.html#lua_api_custom_entity_on_ground_below_changed", null ],
        [ "custom_entity:on_interaction()", "lua_api_custom_entity.html#lua_api_custom_entity_on_interaction", null ],
        [ "custom_entity:on_interaction_item(item_used)", "lua_api_custom_entity.html#lua_api_custom_entity_on_interaction_item", null ]
      ] ]
    ] ]
];