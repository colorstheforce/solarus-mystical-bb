/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Solarus Quests", "index.html", [
    [ "Solarus 1.6 - Lua API reference", "lua_api.html", "lua_api" ],
    [ "Solarus 1.6 - Quest data files specification", "quest.html", "quest" ],
    [ "How to translate a quest", "translation.html", [
      [ "Images", "translation.html#images", null ],
      [ "Strings", "translation.html#strings", null ],
      [ "Dialogs", "translation.html#dialogs", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"index.html",
"lua_api_enemy.html",
"lua_api_hero.html#lua_api_hero_get_state_object",
"lua_api_map.html#lua_api_map_on_opening_transition_finished",
"lua_api_state.html#lua_api_state_is_visible",
"quest_sprite_data_file.html#quest_sprite_data_file_syntax"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';