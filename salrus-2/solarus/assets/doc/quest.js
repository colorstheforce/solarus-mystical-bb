var quest =
[
    [ "Quest properties file", "quest_properties_file.html", [
      [ "Syntax of the quest properties file", "quest_properties_file.html#quest_properties_file_syntax", null ],
      [ "About the quest size", "quest_properties_file.html#quest_properties_file_quest_size", null ],
      [ "Example", "quest_properties_file.html#quest_properties_file_example", null ]
    ] ],
    [ "Quest database file", "quest_database_file.html", [
      [ "Syntax of the database file", "quest_database_file.html#quest_database_file_syntax", [
        [ "Resources", "quest_database_file.html#quest_database_file_resources", null ],
        [ "File authors and licenses", "quest_database_file.html#quest_database_file_files", null ],
        [ "Example", "quest_database_file.html#quest_database_file_example", null ]
      ] ]
    ] ],
    [ "Main Lua script", "quest_main_script.html", null ],
    [ "Quest logos and icons", "quest_logos.html", [
      [ "Quest logo", "quest_logos.html#quest_logos_logo", null ],
      [ "Quest icons", "quest_logos.html#quest_logos_icons", null ]
    ] ],
    [ "Sounds", "quest_sounds.html", null ],
    [ "Musics", "quest_musics.html", [
      [ "Loop settings", "quest_musics.html#quest_musics_loop", null ]
    ] ],
    [ "Fonts", "quest_fonts.html", [
      [ "Outline fonts", "quest_fonts.html#quest_fonts_outline", null ],
      [ "Bitmap fonts", "quest_fonts.html#quest_fonts_bitmap", null ]
    ] ],
    [ "Translated strings", "quest_language_strings.html", null ],
    [ "Translated dialogs", "quest_language_dialogs.html", null ],
    [ "Sprite data file", "quest_sprite_data_file.html", [
      [ "Overview", "quest_sprite_data_file.html#quest_sprite_data_file_overview", null ],
      [ "Origin point", "quest_sprite_data_file.html#quest_sprite_data_file_origin", null ],
      [ "Syntax of a sprite sheet file", "quest_sprite_data_file.html#quest_sprite_data_file_syntax", null ]
    ] ],
    [ "Map definition file", "quest_map_data_file.html", [
      [ "Syntax of the map data file", "quest_map_data_file.html#quest_map_data_file_syntax", [
        [ "Map properties", "quest_map_data_file.html#quest_map_data_file_syntax_properties", null ],
        [ "Declaration of map entities", "quest_map_data_file.html#quest_map_data_file_syntax_entities", [
          [ "Common properties", "quest_map_data_file.html#quest_map_data_file_syntax_common", null ],
          [ "Tile", "quest_map_data_file.html#quest_map_data_file_syntax_tile", null ],
          [ "Dynamic tile", "quest_map_data_file.html#quest_map_data_file_syntax_dynamic_tile", null ],
          [ "Teletransporter", "quest_map_data_file.html#quest_map_data_file_syntax_teletransporter", null ],
          [ "Destination", "quest_map_data_file.html#quest_map_data_file_syntax_destination", null ],
          [ "Pickable treasure", "quest_map_data_file.html#quest_map_data_file_syntax_pickable", null ],
          [ "Destructible object", "quest_map_data_file.html#quest_map_data_file_syntax_destructible", null ],
          [ "Chest", "quest_map_data_file.html#quest_map_data_file_syntax_chest", null ],
          [ "Shop treasure", "quest_map_data_file.html#quest_map_data_file_syntax_shop_treasure", null ],
          [ "Enemy", "quest_map_data_file.html#quest_map_data_file_syntax_enemy", null ],
          [ "Non-playing character", "quest_map_data_file.html#quest_map_data_file_syntax_npc", null ],
          [ "Block", "quest_map_data_file.html#quest_map_data_file_syntax_block", null ],
          [ "Jumper", "quest_map_data_file.html#quest_map_data_file_syntax_jumper", null ],
          [ "Switch", "quest_map_data_file.html#quest_map_data_file_syntax_switch", null ],
          [ "Sensor", "quest_map_data_file.html#quest_map_data_file_syntax_sensor", null ],
          [ "Wall", "quest_map_data_file.html#quest_map_data_file_syntax_wall", null ],
          [ "Crystal", "quest_map_data_file.html#quest_map_data_file_syntax_crystal", null ],
          [ "Crystal block", "quest_map_data_file.html#quest_map_data_file_syntax_crystal_block", null ],
          [ "Stream", "quest_map_data_file.html#quest_map_data_file_syntax_stream", null ],
          [ "Door", "quest_map_data_file.html#quest_map_data_file_syntax_door", null ],
          [ "Stairs", "quest_map_data_file.html#quest_map_data_file_syntax_stairs", null ],
          [ "Separator", "quest_map_data_file.html#quest_map_data_file_syntax_separator", null ],
          [ "Custom entity", "quest_map_data_file.html#quest_map_data_file_syntax_custom_entity", null ]
        ] ]
      ] ]
    ] ],
    [ "Tileset definition file", "quest_tileset_data_file.html", [
      [ "Syntax of the tileset data file", "quest_tileset_data_file.html#quest_tileset_data_file_syntax", [
        [ "Background color", "quest_tileset_data_file.html#quest_tileset_data_file_syntax_background_color", null ],
        [ "Tile patterns", "quest_tileset_data_file.html#quest_tileset_data_file_syntax_tile_patterns", null ],
        [ "Border sets", "quest_tileset_data_file.html#quest_tileset_data_file_syntax_border_set", null ],
        [ "Tileset data file example", "quest_tileset_data_file.html#quest_tileset_data_file_syntax_example", null ]
      ] ]
    ] ],
    [ "Shader description file", "quest_shader_data_file.html", [
      [ "Syntax of the shader description file", "quest_shader_data_file.html#quest_shader_data_file_syntax", null ]
    ] ]
];